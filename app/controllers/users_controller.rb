class UsersController < ApplicationController
  before_action :logged_in_user, only: %i[index edit update destroy]
  before_action :correct_user, only: %i[edit update]
  before_action :admin_user, only: :destroy
  before_action :find_user, only: %i[show correct_user edit update destroy]

  def index
    @users = User.paginate page: params[:page]
  end

  def show
    @user = User.find(params[:id])
    @microposts = @user.microposts.paginate(page: params[:page])
  end

  def logged_in_user
    unless logged_in?
      store_location
      flash[:danger] = t '.edit.please_login'
      redirect_to login_url
    end
  end

  def correct_user
    redirect_to(root_url) unless current_user?(@user)
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      @user.send_activation_email
      flash[:info] = t 'user_mailer.please_check_your_email_to_activate_your_account'
      redirect_to login_url
    else
      flash[:danger] = t '.new.please_fill_entire_information'
      render :new
    end
    flash[:danger] = nil
  end

  def edit; end

  def update
    if @user.update(user_params)
      flash[:success] = t '.new.update_success'
      redirect_to @user
    else
      render :edit
    end
  end

  def destroy
    @user.destroy
    flash[:success] = t '._user.user_deleted'
    redirect_to root_url
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password,
                                 :password_confirmation)
  end

  def admin_user
    redirect_to(root_url) unless current_user.admin?
  end

  def find_user
    @user = User.find(params[:id])
  end
end
