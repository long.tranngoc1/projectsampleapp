class PasswordResetsController < ApplicationController
  before_action :get_user, only: %i[edit update]
  before_action :valid_user, only: %i[edit update]
  before_action :check_expiration, only: %i[edit update]

  def new; end

  def create
    @user = User.find_by(email: params[:password_reset][:email].downcase)
    if @user
      @user.create_reset_digest
      @user.send_password_reset_email
      flash[:info] = t('password_resets.email_send')
      redirect_to root_url
    else
      flash.now[:danger] = t('password_resets.email_address_not_found')
      render :new
    end
  end

  def update
    if params[:user][:password].empty?
      @user.errors.add :password, t('password_resets.cant_empty')
      render :edit
    elsif @user.update(user_params)
      log_in @user
      flash[:success] = t('password_resets.password_has_been_reset')
      redirect_to @user
    else
      render :edit
    end
  end

  def edit; end

  def check_expiration
    if @user.password_reset_expired?
      flash[:danger] = t('password_resets.password_reset_expire')
      redirect_to new_password_reset_url
    end
  end

  private

  def user_params
    params.require(:user).permit :password, :password_confirmation
  end

  def get_user
    @user = User.find_by(email: params[:email])
  end

  def valid_user
    redirect_to root_url unless @user&.activated? && @user&.authenticated?(:reset, params[:id])
  end
end
