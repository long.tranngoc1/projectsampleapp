class AccountActivationsController < ApplicationController
  def edit
    user = User.find_by email: params[:email]
    if user&.authenticated?(:activation, params[:id]) && !user.activated?
      user.update_attribute :activated, true
      user.update_attribute :activated_at, Time.zone.now
      log_in user
      flash[:success] = t 'user_mailer.account_activate'
      redirect_to user
    else
      flash[:danger] = t 'user_mailer.invalid_activate'
      redirect_to root_url
    end
  end
end
