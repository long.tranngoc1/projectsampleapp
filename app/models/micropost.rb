class Micropost < ApplicationRecord
  belongs_to :user
  has_one_attached :image
  scope :newest, -> { order(created_at: :desc) }
  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: Settings.validate.micropost.max_length_content }
  validates :image, content_type: { in: Settings.validate.micropost.type_file,
                                    message: 'must be a valid image format' },
                    size: { less_than: 5.megabytes,
                            message: 'should be less than 5MB' }

  # Returns a resized image for display.
  def display_image
    image.variant(resize_to_limit: Settings.validate.micropost.resize_to_limit)
  end
end
